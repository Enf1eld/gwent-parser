module.exports = function Card() {
    this.uuid = null;           //uuid4
    this.name = null;           //string
    this.reference = null;      //relative url
    this.image = null;          //absolute url
    this.imageThumbnail = null; //absolute url
    this.type = null;           //string: bronze, silver, gold, leader
    this.faction = null;        //string: neutral, northern realms, scoiatael, monsters, skellige
    this.power = null;          //number: [0, +inf)
    this.rarity = null;         //string: common, rare, epic, legendary
    this.row = null;            //array: [melee, ranged, siege, special]
    this.loyalty = null;        //boolean
    this.craft = null;          //object: {normal:{scraps: , meteorite: }, premium:{scraps: , meteorite: }}
    this.mill = null;           //object: {normal:{scraps: , meteorite: }, premium:{scraps: , meteorite: }}
    this.text = null;           //string
    this.flavor = null;         //string
    this.uncollectible = null;  //boolean
}