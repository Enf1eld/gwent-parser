const fs = require('fs'),
    request = require('request'),
    http = require('http');

function initFolders() {
    //fucking shame
    if (!fs.existsSync('images')) fs.mkdirSync('images');
    if (!fs.existsSync('images/Monsters')) fs.mkdirSync('images/Monsters');
    if (!fs.existsSync('images/Scoiatael')) fs.mkdirSync('images/Scoiatael');
    if (!fs.existsSync('images/Skellige')) fs.mkdirSync('images/Skellige');
    if (!fs.existsSync('images/Northern Realms')) fs.mkdirSync('images/Northern Realms');
    if (!fs.existsSync('images/Nilfgaard')) fs.mkdirSync('images/Nilfgaard');
    if (!fs.existsSync('images/Neutral')) fs.mkdirSync('images/Neutral');

    if (!fs.existsSync('images/Monsters/thumbnails')) fs.mkdirSync('images/Monsters/thumbnails');
    if (!fs.existsSync('images/Scoiatael/thumbnails')) fs.mkdirSync('images/Scoiatael/thumbnails');
    if (!fs.existsSync('images/Skellige/thumbnails')) fs.mkdirSync('images/Skellige/thumbnails');
    if (!fs.existsSync('images/Northern Realms/thumbnails')) fs.mkdirSync('images/Northern Realms/thumbnails');
    if (!fs.existsSync('images/Nilfgaard/thumbnails')) fs.mkdirSync('images/Nilfgaard/thumbnails');
    if (!fs.existsSync('images/Neutral/thumbnails')) fs.mkdirSync('images/Neutral/thumbnails');
}


//недогружает файлы
function getCardImages(card) {
    console.log(card.name, card.image, card.imageThumbnail);
    request(card.image).pipe(fs.createWriteStream(`images/${card.faction}/${card.name}.png`));
    request(card.imageThumbnail).pipe(fs.createWriteStream(`images/${card.faction}/thumbnails/${card.name}.png`));
}

initFolders();

let cards = JSON.parse(fs.readFileSync('cards.json', 'utf8'));

cards.forEach(card => getCardImages(card));