const request = require("request"),
    cheerio = require("cheerio"),
    uuid = require("uuid4"),
    fs = require("fs"),
    Card = require("./Card");

setTimeout(() => {
    console.log('start');
    getCountPages().
        then(countPages => {
            parseAllTablePages(countPages).
                then(cards => {
                    getPromisesSecondParseCard(cards).then(cards => {
                        fs.writeFileSync('cards.json', JSON.stringify(cards));
                    })
                })
        })
}, 0)

function parseAllTablePages(numberLastPage) {
    let promise = new Promise((resolve, reject) => {
        startUrl = 'http://www.gwentdb.com/cards?filter-display=1&filter-token=y&page=';
        let promises = [];
        for (let i = 1; i < numberLastPage + 1; i++) {
            promises.push(parseTablePage(startUrl + i));
        }
        Promise.all(promises).then(result => {
            resolve(myConcat(result));
        });
    });
    return promise;
}

//парсим таблицу с указанного url
function parseTablePage(url) {
    let promise = new Promise((resolve, reject) => {
        request(url, (error, response, body) => {
            if (!error) {
                let cards = [],
                    $ = cheerio.load(body),
                    rows = $('table.listing-cards-table>tbody>tr');
                rows.each(function (i, elem) {
                    cards.push(parseCardFirstPart(elem));
                });
                resolve(cards);
            } else {
                console.log('Ошибка: ' + error);
            }
        })
    })
    return promise;
}

//вырезаются данные карточки из таблицы
function parseCardFirstPart(tr) {
    const $ = cheerio.load(tr);
    let card = new Card();
    card.uuid = uuid();
    card.name = $('.col-name').text();
    card.reference = $('.col-name>a').attr('href');
    card.faction = $('.col-faction').text().replace('\'', '');
    card.power = $('.col-power').text();
    card.row = $('.col-row').text().split(', ');
    card.type = $('.col-type').text();
    card.loyalty = $('.col-loyalty').text() ? true : false;

    return card;
}

function getPromisesSecondParseCard(cards) {
    let promise = new Promise((resolve, reject) => {
        let promises = [];
        cards = cards.forEach(card => {
            promises.push(parseCardSecondPart(card));
        })
        Promise.all(promises).then(result => { resolve(result) })
    })
    return promise;
}

//вырезаются недостающие данные карточки из страницы карточки
function parseCardSecondPart(card) {
    const url = 'http://www.gwentdb.com' + card.reference;
    let promise = new Promise((resolve, reject) => {
        request(url, (error, response, body) => {
            const $ = cheerio.load(body);
            card.image = $('.card-image > a').attr('href');
            card.imageThumbnail = $('.card-image img').attr('src');
            card.rarity = $('.card-rarity>span').last().text();
            card.craft = {
                normal: {
                    scraps: $('.card-crafting-cost').first().text().trim(),
                    metheorite: null
                },
                premium: {
                    scraps: $('.card-crafting-cost').last().text().trim().split(' ')[0],
                    metheorite: null
                }
            };
            card.mill = {
                normal: {
                    scraps: $('.card-milling-outcome').first().text().trim(),
                    metheorite: null
                },
                premium: parseMill($('.card-milling-outcome').last().html())
            };
            card.text = $('.card-abilities').text().trim();
            card.flavor = $('.sw-card-flavor-text').text().trim();
            resolve(card);
        })
    })
    return promise;
}

//HELPERS
function getCountPages() {
    let promise = new Promise((resolve, reject) => {
        const url = "http://www.gwentdb.com/cards/";
        request(url, (error, response, body) => {
            if (!error) {
                let $ = cheerio.load(body);
                resolve(Number($('div.listing-footer ul li').eq(-2).text()));
            } else {
                console.log('Ошибка: ' + error);
            }
        })
    })
    return promise;
}

//парсим mill карточки
function parseMill(html) {
    //'<span>10<span class="resource-icon res-scraps size-32"></span>50<span class="resource-icon res-meteorite size-32"></span> (Premium)</span>'
    let result = null;
    if (html) {
        html = html.trim();
        let scraps = html.substring(html.indexOf('<span>') + 6, html.indexOf('<span ')),
            metheorite = html.substring(html.indexOf('</span>') + 7, html.lastIndexOf('<span '));
        result = { scraps, metheorite };
    }
    return result;
}

function myConcat(arrays) {
    let resultArray = [];
    arrays.forEach((currentArray) => {
        for (let i = 0; i < currentArray.length; i++) resultArray.push(currentArray[i]);
    })
    return resultArray;
}