module.exports = function Card() {
        this.reference = null;
        this.name = null;
        this.image = null;
        this.imageThumb = null;
        this.strength = null;
        this.group = null;
        this.rarity = null;
        this.loyalty = null;    //несколько
        this.faction = null;
        this.position = null;
        this.type = null;   //несколько
        this.craft = null;
        this.mill = null;
        this.text = null;
        this.flavor = null;
        this.uncollectible = null;
    }