const request = require("request"),
    cheerio = require("cheerio"),
    fs = require("fs"),
    Card = require("./Card"),
    URL = "http://gwentify.com/cards/",
    SUFFIX = '?view=table';

getCountPages().
    then(countPages => {
        parseAllTablePages(countPages).
            then(links => {
                getCardsInfo(links).
                    then(cards => {
                        fs.writeFileSync('cards.json', JSON.stringify(cards));
                    })
            })
    });

// parseCardInfo('http://gwentify.com/cards/ciaran/').then(result=>{
//     console.log(result);
// })

function parseAllTablePages(countPages) {
    let promise = new Promise((resolve, reject) => {
        let promises = [];
        for (let i = 1; i < countPages + 1; i++) {
            promises.push(parseLinksFromPage(URL + 'page/' + i + '/' + SUFFIX));
        }
        Promise.all(promises).then(result => {
            resolve(myConcat(result));
        });
    });
    return promise;
}

function parseLinksFromPage(url) {
    let promise = new Promise((resolve, reject) => {
        request(url, (error, response, body) => {
            if (!error) {
                let links = [],
                    $ = cheerio.load(body),
                    anchors = $('table a.card-link');
                anchors.each(function (i, elem) {
                    links.push($(this).attr('href'));
                });
                resolve(links);
            } else {
                console.log(error);
            }
        })
    })
    return promise;
}

function getCardsInfo(links) {
    let promise = new Promise((resolve, reject) => {
        let promises = [];
        links = links.forEach(card => {
            promises.push(parseCardInfo(card));
        })
        Promise.all(promises).then(result => { resolve(result) })
    })
    return promise;
}

function parseCardInfo(url) {
    let promise = new Promise((resolve, reject) => {
        request(url, (error, response, body) => {
            const $ = cheerio.load(body);
            let card = new Card();
            card.reference = url;
            card.name = $('h1.entry-title').text();
            card.image = $('div.card-img>a').attr('href');
            card.imageThumb = $('div.card-img img').attr('src');

            let cardCats = $('ul.card-cats li');
            cardCats.each((i, elem) => {
                let text = $(elem).text();
                if (~(text.indexOf('Strength'))) {
                    card.strength = Number(text.split(':')[1].trim());
                }
                if (~(text.indexOf('Group'))) {
                    card.group = text.split(':')[1].trim();
                }
                if (~(text.indexOf('Rarity'))) {
                    card.rarity = text.split(':')[1].trim();
                }
                if (~(text.indexOf('Loyalty'))) {
                    card.loyalty = text.split(':')[1].trim().split(',');
                }
                if (~(text.indexOf('Faction'))) {
                    card.faction = text.split(':')[1].trim();
                }
                if (~(text.indexOf('Position'))) {
                    card.position = text.split(':')[1].trim();
                }
                if (~(text.indexOf('Type'))) {
                    card.type = text.split(':')[1].trim().split(',');
                }
                if (~(text.indexOf('Craft'))) {
                    card.craft = text.split(':')[1].trim();
                }
                if (~(text.indexOf('Mill'))) {
                    card.mill = text.split(':')[1].trim();
                }
            })
            card.text = $('div.card-text').text();
            card.flavor = $('p.flavor').text();
            let uncollectible = $('div.col-md-14>strong>a').text().trim();
            card.uncollectible = (uncollectible === 'Uncollectible') ? true : false;
            resolve(card);
        })
    })
    return promise;
}

function parseCardFirstPart(tr) {
    const $ = cheerio.load(tr);
    let card = new Card();
    card.name = $('.col-name').text();
    card.reference = $('.col-name>a').attr('href');
    card.faction = $('.col-faction').text();
    card.power = $('.col-power').text();
    card.row = $('.col-row').text();
    card.type = $('.col-type').text();
    card.loyalty = $('.col-loyalty').text();

    return card;
}

//HELPERS
function getCountPages() {
    let url = URL + (SUFFIX ? SUFFIX : '');
    let promise = new Promise((resolve, reject) => {
        request(url, (error, response, body) => {
            if (!error) {
                let $ = cheerio.load(body);
                var hrefArray = $('nav.text-center>ul.pagination-centered>li:last-child>a').attr('href').split('/');
                countPages = Number(hrefArray[hrefArray.length - 2]);
                resolve(countPages);
            } else {
                console.log(error);
            }
        })
    })
    return promise;

}

function myConcat(array) {
    let resultArray = [];
    array.forEach((currentArray, index) => {
        for (let i = 0; i < currentArray.length; i++) resultArray.push(currentArray[i]);
    })
    return resultArray;
}